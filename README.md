# L6 Git Exercise
This microHAT use case is a motion sensor with a lighting controller whereby 
it will detect movement and the Pi will control a set of lights independently.
A scenario would be if someone wanted to save power in their home with lights 
that reacted to people being in a room and adjacent rooms, the microHAT can 
detect which room the person is in and illuminate the current and nearby rooms.

Subsystems of the PiHAT are:
-Power Regulator-
This system will control the voltage supply of the lights that the Pi will turn 
on and the supply of the IR sensors sending input to the Pi. These subsystems 
operate at different voltages so the power regulator will need to provide different 
power supplies; 12V for the LED lamps and 3.3V for the IR sensor. A 12V zener diode 
will control a Transistor to regulate the 12V rail and a linear voltage regulator
(LT1086CT-3.3#PBF) will regulate the 3.3V rail.

-IR Sensor-
This will take the 3.3V supply from the Power regulator and output a digital 
0-3.3V signal to the Pi GPIO port when the sensor detects an environmental 
stimulus. The HC-SR501 IR sensor will output a logic high when it detects a 
change in the infrared spectrum, this input is converted to a digital 0-3V3 
input signal which is sent to the Pi input. When the IR sensor is triggered, 
it will output a signal to the LED indicators to display the trigger state.

-Lights-
This is the system of lights that will be turned on when motion is detected. 
They will receive a 12V supply from the power regulator and switch on/off 
when the signal is sent from the Pi. When the lights are on, the LED indicators
also need to turn on to indicate the mode of the light. There will be 3 lights 
and will be switched by a IRF60A mosfet (HAT2027R).

-LED Indicators-
These LED indicators will interact with the lights and the IR sensor. When the 
IR sensor detects motion, and indicator light corresponding to the sensor will 
activate. When no motion is detected, this indicator will remain off. When the 
lights are on, corresponding indicators will turn on as well. When the lights 
turn off, the indicators connected to the lights will also turn off. Each 
indicator will use a BL-B4634 LED.

-Trigger Switct-
This subsystem will trigger an external circuit by the use of a 1 amp T81H5D312-12 
relay. The subsystem will receive an input signal from the Pi and short two external 
connectors to trigger a simple external trigger system.
